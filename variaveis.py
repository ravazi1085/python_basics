# resto de divisao
mod = 7 % 2
print('MOD 7 % 2: ', mod)

# repete string 3 vezes
str = 'STRING '
print('str * 3', str * 3)

# maisculo
print('str upper', str.upper())

# capitalizado
print('str cap', str.capitalize())

# entrada usuario
_input = input('Nome: ')
print('Nome Inserido: {}' .format(_input))