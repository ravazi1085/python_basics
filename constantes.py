# principais consts = True, False e None

num1 = 1
num2 = 2

# comparacao
print(num1 == num2)

# converte qualquer literal em bool
# !!_var em JS
print(bool(-1))

# operadores comparacao 
# == != < > <= >=

# operadores python
# a is b
# a is not b
# a in b #objetos
# a not in b #objetos

